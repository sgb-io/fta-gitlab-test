#!/usr/bin/env node

const { execSync } = require("node:child_process");
const path = require("node:path");
const fs = require("node:fs");

const platform = process.platform;

function getBinaryPath() {
  return path.join(__dirname, "fta");
}

function setUnixPerms(binaryPath) {
  if (platform === "darwin" || platform === "linux") {
    try {
      fs.chmodSync(binaryPath, "755");
    } catch (e) {
      console.warn("Could not chmod fta binary: ", e);
    }
  }
}

// Run the binary from code
// We build arguments that get sent to the binary
function runFta(project, options) {
  const binaryPath = getBinaryPath();
  const binaryArgs = options.json ? "--json" : "";
  setUnixPerms(binaryPath);
  const result = execSync(`${binaryPath} ${project} ${binaryArgs}`);
  return result.toString();
}

// Run the binary directly if executed as a standalone script
// Arguments are directly forwarded to the binary
if (require.main === module) {
  const args = process.argv.slice(2); // Exclude the first two arguments (node binary and script file)
  const binaryPath = getBinaryPath();
  const binaryArgs = args.join(" ");
  setUnixPerms(binaryPath);
  const result = execSync(`${binaryPath} ${binaryArgs}`);
  console.log(result.toString());
}

module.exports.runFta = runFta;
